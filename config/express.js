var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var fs = require('fs');

module.exports = function(app, config) {
app.locals.title = "The Crossing Church";

app.set('views', config.root + '/public');
    app.engine('html', function(file_path, options, callback) {
        fs.readFile(file_path, function(err, content) {
            if(err) return callback(new Error(err));
            callback(null, content.toString());
        });
    });
    app.set('view engine', 'html');

    var env = process.env.NODE_ENV || 'development';
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env == 'development';

    // app.use(favicon(config.root + '/public/img/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
    extended: true
    }));
    //app.use(cookieParser());
    app.use(compress());
    app.use(express.static(config.root + '/public'));
    app.use(methodOverride());
    app.use(session({
      store: new RedisStore({
          host: 'localhost',
          ttl: 2629800,
          prefix: 'tccsess:'
      }),
      secret: 'There be dragons',
      cookie: {
          maxAge: 31535999 * 1000,
          httpOnly: false,
          rolling: true
      }
    }));

    var controllers = glob.sync(config.root + '/app/controllers/*.js');
    controllers.forEach(function (controller) {
    require(controller)(app);
    });

    app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
    });

    if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
    }

    app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
    });

};
