var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'tccadmin'
    },
    port: 3000,
    //db: 'postgres://tccleaders:asdfasdf@localhost/tcc'
    db: 'postgres://tccleaders:asdfasdf@tccleaders.coalescent.technology/tcc'
  },

  test: {
    root: rootPath,
    app: {
      name: 'tccadmin'
    },
    port: 3000,
    db: 'postgres://tccleaders:asdfasdf@localhost/tcc'
  },

  production: {
    root: rootPath,
    app: {
      name: 'tccadmin'
    },
    port: 3001,
    db: 'postgres://tccleaders:asdfasdf@localhost/tcc'
  }
};

module.exports = config[env];
