'use strict';

var request = require('request');

var config = require('./config/config');

module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    var reloadPort = 35729, files;

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        develop: {
            server: {
                file: 'server.js'
            }
        },
        browserify: {
            dev: {
                options: {
                    debug: true,
                    transform: ['reactify']
                },
                files: {
                    'public/script/main.js': 'src/**/*.jsx'
                }
            },
            build: {
                options: {
                    debug: false,
                    transform: ['reactify']
                },
                files: {
                    'public/script/main.js': 'scr/**/*.jsx'
                }
            }
        },
        sproc: {
            default: {
                options: {
                    db: config.db
                },
                files: [{
                    src: 'sprocs/**/*.sproc.sql'
                }]
            }
        },
        watch: {
            options: {
                nospawn: true,
                livereload: reloadPort
            },
            browserify: {
                files: ['src/**/*.jsx'],
                tasks: ['browserify:dev']
            }
        }
    });

    grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
        var done = this.async();
        setTimeout(function () {
            request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','), function (err, res) {
                var reloaded = !err && res.statusCode === 200;
                if (reloaded)
                    grunt.log.ok('Delayed live reload successful.');
                else
                    grunt.log.error('Unable to make a delayed live reload.');
                done(reloaded);
            });
        }, 500);
    });

    grunt.registerTask('build', [
        'browserify:build'
    ]);

    grunt.registerTask('default', [
        'browserify:dev',
        'develop',
        'watch'
    ]);
};
