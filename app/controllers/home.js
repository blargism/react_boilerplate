var express = require('express'),
	router = express.Router(),
	async = require('async'),
	moment = require('moment'),
	sproc = require('../services/sproc'),
	auth = require('../middleware/auth');

module.exports = function (app) {
	app.use('/', router);
};

moment.locale('en', {
	calendar: {
		lastDay: '[Yesterday at] LT',
		sameDay: '[Today at] LT',
		nextDay: '[Tomorrow at] LT',
		lastWeek: '[last] dddd [at] LT',
		nextWeek: 'dddd [at] LT',
		sameElse: 'L [at] LT'
	}
});

router.get('/', function (req, res, next) {
    res.render('home');
});
