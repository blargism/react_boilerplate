var express = require('express'),
	router = express.Router(),
	sproc = require('../services/sproc'),
	auth = require('../middleware/auth');


module.exports = function (app) {
	app.use('/', router);
};

router.get('/login', function (req, res, next) {
	res.sendFile('login.html');
});
