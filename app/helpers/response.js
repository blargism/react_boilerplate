module.exports = function(res) {
  return function(err, data) {
    if(err) return res.status(500).json({error: err});
    if(!data) return res.status(404).json({error: "Not found"});

    res.json(data);
  }
};
