var pg = require('pg'),
    config = require('../../config/config');

var regex = /LIMIT/i;

function query(queryString, params, cb) {
  pg.connect(config.db, function(err, pgClient, release) {
    var i;
    var count = params.length;
    var args = [];

    pgClient.query(queryString, params, function(err, result) {
      release();
      if(err) {
        cb(err);
      } else {
        cb(null, result.rows, result);
      }
    })
  });
}

function one(queryString, params, cb) {
  if(!queryString.match(regex)) {
    queryString += " LIMIT 1";
  }

  query(queryString, params, function(err, rows, result) {
    cb(err, rows[0], result);
  });
}

module.exports = {
  one: one,
  many: query
};
