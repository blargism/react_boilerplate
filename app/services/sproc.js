/**
 * Created by joemills on 3/29/15.
 */

var pg = require('pg'),
    config = require('../../config/config');

function runSproc(sproc, params, cb) {
  pg.connect(config.db, function(err, pgClient, release) {
    var i;
    var count = params.length;
    var args = [];

    for(i = 1; i <= count; i++) {
      args.push('$'+i);
    }

    var query = 'SELECT * FROM tccleaders.' + sproc + '(' + args.join(', ') + ')';

    pgClient.query(query, params, function(err, result) {
      release();
      if(err) {
        cb(err);
      } else {
        cb(null, result.rows, result);
      }
    })
  });
}

function getSingle(sproc, params, cb) {
  runSproc(sproc, params, function(err, rows, result) {
    if(!rows || rows.length < 1) {
      return cb(err, {}, result);
    }
    cb(err, rows[0], result);
  });
}

function execCmd(sproc, params, cb) {
  runSproc(sproc, params, function(err, rows, result) {
    cb(err, result);
  });
}

module.exports = {
    getMany: runSproc,
    getOne: getSingle,
    exec: execCmd
}
