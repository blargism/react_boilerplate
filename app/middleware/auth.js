var sproc = require('../services/sproc');

function userIn(req, res, next) {
  var user_id = req.session ? req.session.user_id : null;
  sproc.getOne('users_get', [user_id || null], function (err, user) {
    if (err || !user.user_id) {
      res.redirect('/login');
      return;
    }

    req.user = user;

    next();
  });
}

function login(req, success, failure) {
  var username = req.body.username;
  var password = req.body.password;

  sproc.getOne('auth_login', [username, password], function (err, user) {
    if (err || !user.user_id) {
      return failure('Your username and password are not correct');
    }

    req.session.user_id = user.user_id;
    success(user);
  });
}

module.exports = {
  userIn: userIn,
  login: login
};
