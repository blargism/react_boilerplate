var React = require('react');
var ReactDOM = require('react-dom');

var EmptyComponent = React.createClass({
    render: function() {
        return (
            <div>Nothing here</div>
        )
    }
});

module.exports = EmptyComponent;