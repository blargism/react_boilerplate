var React = require('react');
var ReactDOM = require('react-dom');
var EmptyComponent = require('./EmptyComponent.jsx');

class MainContent extends React.Component {
    constructor(props) {
        super(props);
        if(!this.props.title) this.props.title = "Camp";
        
        this.state = {
            nav: {
                home: props.section == 'home' ? 'active' : '',
                find: props.section == 'find' ? 'active' : '',
                mycamp: props.section == 'mycamp' ? 'active' : '',
                about: props.section == 'about' ? 'active' : '',
                contact: props.section == 'contact' ? 'active' : '',
            }
        }
    }
    
    componentWillReceiveProps(props) {
        this.setState({
            nav: {
                home: props.section == 'home' ? 'active' : '',
                find: props.section == 'find' ? 'active' : '',
                mycamp: props.section == 'mycamp' ? 'active' : '',
                about: props.section == 'about' ? 'active' : '',
                contact: props.section == 'contact' ? 'active' : '',
            }
        });
    }
    
    render() {
        var PageComponent = this.props.component || "EmptyComponent";
        return (
            <div>
                <div className="masthead">
                    <h3 className="text-muted">Project name</h3>
                    <nav>
                        <ul className="nav nav-justified">
                            <li className={this.state.nav.home}><a href="#">Home</a></li>
                            <li className={this.state.nav.find}><a href="#/find">Find A Camp</a></li>
                            <li className={this.state.nav.mycamp}><a href="#">My Camp</a></li>
                            <li className={this.state.nav.about}><a href="#">About</a></li>
                            <li className={this.state.nav.contact}><a href="#">Contact</a></li>
                        </ul>
                    </nav>
                </div>

                <div id="content" className="container">
                    <PageComponent {...this.props} />
                </div>
            </div>
        )
    }
}

module.exports = MainContent;