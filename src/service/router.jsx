var React = require('react');
var ReactDOM = require('react-dom');
var MainContent = require('../component/MainContent.jsx');
var MainContentFactory = React.createFactory(MainContent);
var NotFoundContent = require('../component/NotFoundContent.jsx');
var _ = require('lodash');
var routes = [];
var optional_param_rgx = /\((.*?)\)/g;
var named_param_rgx = /(\(\?)?:\w+/g;
var named_param_array_rgx = /(:\w+)/g;
var splat_param_rgx = /\*\w+/g;
var escape_rgx  = /[\-{}\[\]+?.,\\\^$|#\s]/g;

window.addEventListener('hashchange', function(evt) {
    var hash = evt.newURL.split('#')[1];
        
    Router.go(hash, evt);
});

function extractParams(hash, rgx) {
    var params = rgx.exec(hash).slice(1);
    return _.map(params, function(param, i) {
        if(i === params.length - 1) return param || null;
        return param ? decodeURIComponent(param) : null;
    });
}

var Router = {
    go: function(hash, evt) {
        if(hash.substring(0,1) != '/') {
            location.hash = '/' + hash;
            return;
        }
        
        if(!evt) evt = {};
        
        for(var i = 0; i < routes.length; i++) {
            if(routes[i].static && routes[i].route == hash) {
                return routes[i].handler(evt, []);
            } else if(routes[i].route.test(hash)) {
                var params = {};
                var param_names = routes[i].params;
                var param_values = extractParams(hash, routes[i].route);
                
                if(param_names) {
                    for(var i = 0; i < param_names.length; i++) {
                        params[param_names[i]] = param_values[i];
                    }
                }
                
                return routes[i].handler(evt, params);
            }
        }
        
        ReactDOM.render(
            <NotFoundContent />,
            document.getElementById('content')
        );
    },
    
    add: function(route, config, callback) {
        if(!callback) callback = config;
        if(!callback) new Error("No callback provided");
        var route_rgx, params;
        
        if(route.substring(0,1) != '/') {
            route = '/' + route;
        }
        
        if(config.static) {
            route_rgx = route;
        } else {
            // Credit to backbone's regex
            route_rgx = new RegExp(
                '^' +
                route.replace(escape_rgx, '\\$&')
                    .replace(optional_param_rgx, '(?:$1)?')
                    .replace(named_param_rgx, function(match, optional) {
                        return optional ? match : '([^/?]+)';
                    })
                    .replace(splat_param_rgx, '([^?]*?)') +
                '(?:\\?([\\s\\S]*))?$'
            );
            
            params = route.match(named_param_array_rgx);
            if(params) {
                for(var i = 0; i<params.length; i++) {
                    params[i] = params[i].replace(':', '');
                }
            }
        }
        
        routes.push({
            handler:function(evt, props) {
                Component = MainContentFactory(props, callback(evt, props));
                ReactDOM.render(
                    Component,
                    document.getElementById('content')
                );
            },
            params: params,
            route: route_rgx,
            static: config.static ? true : false
        });
    }
}

module.exports = Router;