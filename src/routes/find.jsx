var React = require('react');
var Router = require('../service/router.jsx');

class Find extends React.Component {
    render() {
        return (
            <section>
                <div className="jumbotron">
                    <h1>Find Your Camp</h1>
                    <p className="lead">
                        Finding the right camp is really easy. 
                    </p>
                    <p>
                        You can start one of two ways.  You can either look up your church or pick a specific camp.
                    </p>
                </div>
            </section>
        );
    }
}

Router.add('/find', {}, (evt, params) => {
    params.title = 'Find your camp';
    params.section = 'find';
    return <Find {...params} />
});