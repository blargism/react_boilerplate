// The base file for this project
React = require('react');
ReactDOM = require('react-dom');
MainContent = require('./component/MainContent.jsx');
Router = require('./service/router.jsx');

require('./routes/home.jsx');
require('./routes/find.jsx');

Router.go(location.hash.replace('#', '') || "");